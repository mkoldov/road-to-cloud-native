resource "random_string" "db_password" {
  length  = 16
  special = false
}

resource "null_resource" "env-file-db" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = <<EOT
    echo > ./env/db.env
    echo POSTGRES_PASSWORD="${random_string.db_password.result}" >> ./env/db.env
    echo POSTGRES_USER=mypguser >> ./env/db.env
    echo POSTGRES_DB=cloudnative >> ./env/db.env
    echo > ./env/app-db.env
    echo DB_ENDPOINT=db >> ./env/app-db.env
    echo DB_USERNAME=mypguser  >> ./env/app-db.env
    echo DB_PASSWORD="${random_string.db_password.result}"  >> ./env/app-db.env
    echo DB_NAME=cloudnative >> ./env/app-db.env
  EOT
  }
  depends_on = [aws_iam_access_key.s3bucket-user, aws_s3_bucket.assets-bucket]
}

resource "null_resource" "env-file" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = <<EOT
    echo > ./env/s3.env
    echo AWS_REGION=eu-central-1 > ./env/s3.env
    echo ASSETS_BUCKET=${aws_s3_bucket.assets-bucket.bucket} >> ./env/s3.env
    echo AWS_ACCESS_KEY_ID=${aws_iam_access_key.s3bucket-user.id} >> ./env/s3.env
    echo S3_USER_ENC_SECRET=${aws_iam_access_key.s3bucket-user.encrypted_secret} >> ./env/s3.env
    # echo $(echo $S3_USER_ENC_SECRET | base64 -d | gpg --decrypt 2>/dev/null) 
  EOT
  }
  depends_on = [aws_iam_access_key.s3bucket-user, aws_s3_bucket.assets-bucket]
}

