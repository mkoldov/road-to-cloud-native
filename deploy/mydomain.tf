
data "aws_route53_zone" "ecs_domain" {
  name         = var.ecs_domain_name
  private_zone = false
}

resource "aws_route53_record" "ecs_load_balancer_record" {
  name    = "*.${var.ecs_domain_name}"
  type    = "A"
  zone_id = data.aws_route53_zone.ecs_domain.zone_id

  alias {
    evaluate_target_health = false
    name                   = aws_alb.main.dns_name
    zone_id                = aws_alb.main.zone_id
  }
}


# issue certificate e.g. *.lab.undo8.com
# used on ALB HTTPS listener

resource "aws_acm_certificate" "ecs_domain_certificate" {
  domain_name       = "*.${var.ecs_domain_name}"
  validation_method = "DNS"

  tags = {
    Name = "${var.ecs_cluster_name}-Certificate"
  }

  lifecycle {
    create_before_destroy = true
  }
}

// validated by DNS rec below
// it might require aws_acm_certificate.ecs_domain_certificate to be created first by:
//    docker-compose run --rm terraform apply -target aws_acm_certificate.ecs_domain_certificate
resource "aws_route53_record" "ecs_cert_validation_record" {
  for_each = {
    for dvo in aws_acm_certificate.ecs_domain_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.ecs_domain.zone_id

  depends_on = [aws_acm_certificate.ecs_domain_certificate]
}

resource "aws_acm_certificate_validation" "ecs_domain_certificate_validation" {
  certificate_arn = aws_acm_certificate.ecs_domain_certificate.arn
  validation_record_fqdns = [
    for rec in aws_route53_record.ecs_cert_validation_record :
    rec.fqdn
  ]
}