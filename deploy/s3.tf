
resource "random_id" "s3-bucket-id" {
  byte_length = 6

}

resource "aws_s3_bucket" "assets-bucket" {
  bucket        = "${var.name}-${random_id.s3-bucket-id.hex}"
  acl           = "public-read"
  force_destroy = true

  tags = {
    Name        = "My test bucket"
    Environment = "Dev"
  }
}
