output "s3_bucket" {
  value = aws_s3_bucket.assets-bucket.bucket
}

output "s3_bucket_key_id" {
  value = aws_iam_access_key.s3bucket-user.id
}

output "s3_bucket_key_encrypted" {
  value = aws_iam_access_key.s3bucket-user.encrypted_secret
}

output "alb_url" {
  value = aws_alb.main.dns_name
}

// eg. *.lab.undo8.com
output "dns_rec" {
  value = aws_route53_record.ecs_load_balancer_record.fqdn
}
