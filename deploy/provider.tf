provider "aws" {
  region  = var.region
  version = "~> 2.15"
}

provider "random" {}
provider "null" {}
