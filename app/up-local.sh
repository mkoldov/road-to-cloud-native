#!/bin/bash

export S3_SECRET_ACCESS_KEY=$(grep S3_USER_ENC_SECRET ../deploy/env/s3.env | cut -d= -f2 | base64 -d 2>/dev/null | gpg --decrypt 2>/dev/null)
#echo $S3_SECRET_ACCESS_KEY
docker volume rm app_database-data
docker-compose up -d --force-recreate --build