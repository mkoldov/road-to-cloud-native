#!/bin/bash

set -e 

export AWS_REGION=eu-central-1
export REPO_NAME=cloud-native-project

# check dependencies
# - jq
# - AWS cli (tested with v2)
# - docker
if ! [ -x "$(command -v jq)" ]; then
  echo 'Error: jq is not installed. Please install it first: https://stedolan.github.io/jq/' >&2
  exit 1
fi

if ! [ -x "$(command -v aws)" ]; then
  echo 'Error: AWS-CLI is not installed. Please install it first: https://aws.amazon.com/cli/' >&2
  exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
  echo 'Error: Docker is not installed. Please install it first: https://www.docker.com/' >&2
  exit 1
fi

aws ecr describe-repositories --region ${AWS_REGION} | jq -r ".repositories[] | select(.repositoryName==\"${REPO_NAME}\")"
repositoryUri=$(aws ecr describe-repositories --region ${AWS_REGION} | \
 jq -r ".repositories[] | select(.repositoryName==\"${REPO_NAME}\") | .repositoryUri")
echo REPO "${repositoryUri}"
if [ -n "$repositoryUri" ]; then
  echo "Repository found, no need to create one."
else
  echo "Could not find our ECR, creating..."
  repositoryUri=$(aws ecr create-repository --repository-name "${REPO_NAME}" --region ${AWS_REGION} | jq -r .repository.repositoryUri)
fi

# $(aws ecr get-login --no-include-email --region ${AWS_REGION})
# login docker client to ECR
aws --region ${AWS_REGION} ecr get-login-password \
    | docker login \
        --password-stdin \
        --username AWS \
        "${repositoryUri}"

# build app image locally
docker build -t ${REPO_NAME} .
# tag it
docker tag ${REPO_NAME}:latest "${repositoryUri}:latest"
# upload to ECR
docker push "${repositoryUri}:latest"

echo "Done with ${repositoryUri}"
echo "Success!"
