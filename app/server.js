// express web server
const express = require('express');
// accessing Postgres DB
const {
    Pool
} = require('pg');
// and S3 bucket
const {
    S3
} = require('aws-sdk');


const os = require('os');
const fs = require('fs');

const hostname = fs.readFileSync('/etc/hostname', {
    encoding: 'utf8',
    flag: 'r'
});

const nodeInfo = {
    "Hostname": hostname,
    "Platform": os.platform(),
        "Arch": os.arch(),
        "CPU count": os.cpus().length,
        "Uptime": os.uptime(),
        cpus: os.cpus(),
        netIf: os.networkInterfaces() 
}

// DB engine used - by env vars
const pool = new Pool({
    host: process.env.DB_ENDPOINT,
    port: 5432,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
});

const s3 = new S3();


// default port is 3000 or controlled by envvar PORT
const port = process.env.PORT || 3000;

const app = express();

app.get('/', async (req, res) => {
    let data = null;
    let listObjects = null;
    try {
        // test DB
        data = await pool.query('SELECT * FROM pg_catalog.pg_tables;');

        // test S3
        listObjects = await s3.listObjectsV2({
            Bucket: process.env.ASSETS_BUCKET,
        }).promise();

        res.status(200);
        res.send({
            pgTables: data,
            bucketObjects: listObjects,
            nodeInfo
        });
    } catch (err) {
        // report errors
        console.log(err);

        res.status(500);

        return res.send({
            err,
            pgTables: data,
            bucketObjects: listObjects,
            nodeInfo
        });
    }
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));